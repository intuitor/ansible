---
- name: ensure apt cache is up to date
  apt:  update_cache=yes

- name: adding the postgresql repository
  apt_repository:
    repo: deb http://apt.postgresql.org/pub/repos/apt/ xenial-pgdg main
    state: present

- name: add the apt signing key
  apt_key:
    url: https://www.postgresql.org/media/keys/ACCC4CF8.asc
    state: present

- name: ensure packages are installed
  apt:  name={{item}} update_cache=yes
  with_items:
        - postgresql-10
        - libpq-dev
        - python3-psycopg2
- name: ensure database is created
  become_user: postgres
  postgresql_db: name={{ premlocaldb }}

- name: ensure user has access to database
  become_user: postgres
  postgresql_user: db={{ premlocaldb }} name={{ dbuser }} password={{ dbpassword }} priv=ALL encrypted=yes

- name: ensure user does not have unnecessary privilege
  become_user: postgres
  postgresql_user: name={{ dbuser }} role_attr_flags=NOSUPERUSER,NOCREATEDB

- name: ensure no other user can access the database
  become_user: postgres
  postgresql_privs: db={{ premlocaldb }} role=PUBLIC type=database priv=ALL state=absent

- lineinfile:
    path: "{{ pg_hba_conf }}"
    insertafter: 'host    all             all             127.0.0.1/32            md5'
    line: 'host    all             all             10.1.10.0/24             trust'

- lineinfile:
    path: "{{ postgresql_conf }}"
    state: present
    regexp: '^#listen_addresses'
    line: "listen_addresses = '*'"
